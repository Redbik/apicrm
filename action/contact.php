<?php
    require_once ("auth.php");

    class Add{
        public $number;
        function __construct(){
            $this->number = $_POST['col'];
        }

        public function addContact(){
            for($i=1; $i<=$this->number; $i++){
                $data[] = array(
                    'name' => 'Contact '.$i,
                    'responsible_user_id' => 504141,
                    'created_by' => 504141,
                    'created_at' => "1509051600",
                    'tags' => "важный,доставка",
                );
            }

            $contacts['add']= $data;


            $subdomain='testdudarev'; #Наш аккаунт - поддомен
            #Формируем ссылку для запроса
            $link='https://'.$subdomain.'.amocrm.ru/api/v2/contacts';
            /* Нам необходимо инициировать запрос к серверу. Воспользуемся библиотекой cURL (поставляется в составе PHP). Подробнее о
            работе с этой
            библиотекой Вы можете прочитать в мануале. */
            $curl=curl_init(); #Сохраняем дескриптор сеанса cURL
            #Устанавливаем необходимые опции для сеанса cURL
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
            curl_setopt($curl,CURLOPT_URL,$link);
            curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
            curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($contacts));
            curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
            curl_setopt($curl,CURLOPT_HEADER,false);
            curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
            curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
            curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
            curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
            $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
            $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
            /* Теперь мы можем обработать ответ, полученный от сервера. Это пример. Вы можете обработать данные своим способом. */
            $code=(int)$code;
            $errors=array(
                301=>'Moved permanently',
                400=>'Bad request',
                401=>'Unauthorized',
                403=>'Forbidden',
                404=>'Not found',
                500=>'Internal server error',
                502=>'Bad gateway',
                503=>'Service unavailable'
            );
            try
            {
                #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
                if($code!=200 && $code!=204) {
                    throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
                }
            }
            catch(Exception $E)
            {
                die('Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode());
            }
            /*
             Данные получаем в формате JSON, поэтому, для получения читаемых данных,
             нам придётся перевести ответ в формат, понятный PHP
             */
            $Response=json_decode($out,true);
            $Response=$Response['_embedded']['items'];
            $output = array();

            foreach($Response as $v)
                if(is_array($v))
                    $output[]=$v['id'];
            echo "Массив ID контактов ";
            print_r($output);
            echo "</br>";

        }

        public function addLeads(){
            for($i=1; $i<=$this->number; $i++){
                $data[] = array(
                    'name'=>'Lead '.$i,
                    'created_at'=>1298904164,
                    'status_id'=>7087609,
                    'sale'=>rand(1000, 1000000),
                    'tags' => 'Important, USA',
                );
            }
            $leads['add'] = $data;

            $subdomain='testdudarev';

            $link='https://'.$subdomain.'.amocrm.ru/api/v2/leads';
            /* Нам необходимо инициировать запрос к серверу. Воспользуемся библиотекой cURL (поставляется в составе PHP). Подробнее о
            работе с этой
            библиотекой Вы можете прочитать в мануале. */
            $curl=curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
            curl_setopt($curl,CURLOPT_URL,$link);
            curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
            curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($leads));
            curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
            curl_setopt($curl,CURLOPT_HEADER,false);
            curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
            curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
            curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
            curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
            $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
            $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
            /* Теперь мы можем обработать ответ, полученный от сервера. Это пример. Вы можете обработать данные своим способом. */
            $code=(int)$code;
            $errors=array(
                301=>'Moved permanently',
                400=>'Bad request',
                401=>'Unauthorized',
                403=>'Forbidden',
                404=>'Not found',
                500=>'Internal server error',
                502=>'Bad gateway',
                503=>'Service unavailable'
            );
            try
            {
                #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
                if($code!=200 && $code!=204) {
                    throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
                }
            }
            catch(Exception $E)
            {
                die('Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode());
            }

            $Response=json_decode($out,true);
            $Response=$Response['_embedded']['items'];
            $output = array();

            foreach($Response as $v)
                if(is_array($v))
                    $output[]=$v['id'];
            echo "Массив ID сделок ";
            print_r($output);
            echo "</br>";
        }
    }

    $obj = new Add;
    $obj->addContact();
    $obj->addLeads();
